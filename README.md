# Task Management API
## Overview
This Task Management API is designed to be able to create, manage, and retrieve hierarchical tasks. 
It supports operations such as adding, updating and deleting tasks, marking tasks as completed,
changing their parents and fetching tasks with their subtasks with the ability to aplly filters and sorting.

## Technologies
**Backend**: Laravel 11, PHP 8.3

**Database**: MySQL 8.4

**Other Tools**:Docker, PHPUnit for testing

## Purpose
This project was developed as part of a coding test for a job application.
The main goal was to create a RESTful API that can manage tasks and subtasks.

## Requirements
|                         |                         |
|-------------------------|-------------------------|
| ![page one](docs/1.jpg) | ![page two](docs/2.jpg) |
