<?php

namespace Database\Factories;

use App\Domain\Models\Task;
use App\Domain\Models\TaskStatus;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends Factory<Task>
 */
class TaskFactory extends Factory
{
    protected $model = Task::class;
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $createdTime = $this->faker->dateTimeThisYear;
        $completed = $this->faker->boolean;

        return [
            'title' => $this->faker->realTextBetween(10, 20),
            'description' => $this->faker->realTextBetween(50, 100),
            'priority' => $this->faker->numberBetween(1, 5),
            'parent_id' => null,
            'status' => $completed ? TaskStatus::DONE : TaskStatus::TODO,
            'completed_at' => $completed ? (clone $createdTime)->modify('+1 day') : null,
            'created_at' => $createdTime,
            'updated_at' => $createdTime,
        ];
    }

    public function completed(): TaskFactory
    {
        return $this->state(function (array $attributes) {
            return [
                'status' => TaskStatus::DONE,
                'completed_at' => (clone $attributes['created_at'])->modify('+1 day')
            ];
        });
    }

    public function notCompleted(): TaskFactory
    {
        return $this->state(function (array $attributes) {
            return [
                'status' => TaskStatus::TODO,
                'completed_at' => null
            ];
        });
    }
}
