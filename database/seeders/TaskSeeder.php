<?php

declare(strict_types=1);

namespace Database\Seeders;

use App\Domain\Models\Task;
use App\Domain\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Collection;
use Database\Factories\TaskFactory;

class TaskSeeder extends Seeder
{
    public function run(): void
    {
        $firstUser = User::find(1);

        $arbitraryUsers = User::query()->inRandomOrder()->take(5)->get();

        $users = $arbitraryUsers->add($firstUser);

        $users->each(fn(User $user) => $this->createTasksForUser($user));
    }

    private function createTasksForUser(User $user): void
    {
        echo "Creating tasks for user with email: {$user->email}\n";

        $tasks = TaskFactory::new()->count(rand(2, 5))->create(['user_id' => $user->id]);

        $subtasks = $tasks;

        for($cycle = 0; $cycle < 4; $cycle++) {
            $subtasks = $subtasks->flatMap(fn(Task $task) => $this->createSubtasks($task));
        }
    }

    private function createSubtasks(Task $task): Collection
    {
        $subtasks = TaskFactory::new()->count(rand(0, 5))->make(['user_id' => $task->getAttribute('user_id')]);

        $task->subtasks()->saveMany($subtasks);

        return $subtasks;
    }
}
