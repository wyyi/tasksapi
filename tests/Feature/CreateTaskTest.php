<?php

declare(strict_types=1);

namespace Tests\Feature;

use Tests\TestCase;
use Database\Factories\UserFactory;
use PHPUnit\Framework\Attributes\Test;

class CreateTaskTest extends TestCase
{
    #[Test]
    public function user_can_create_task_with_valid_data(): void
    {
        $user = UserFactory::new()->create();

        $this->actingAs($user);

        $response = $this->postJson(route('tasks.store'), [
            'title' => 'A very secret task',
            'description' => 'Task\'s secret description'
        ]);

        $response->assertStatus(201);

        $response->assertJsonStructure([
            'data' => ['id', 'title', 'description', 'status', 'priority', 'parent_id', 'created_at', 'updated_at']
        ]);

        $response->assertJson([
            'data' => ['title' => 'A very secret task', 'description' => 'Task\'s secret description']
        ]);

        $this->assertDatabaseHas('tasks', [
            'title' => 'A very secret task',
            'description' => 'Task\'s secret description'
        ]);
    }

    #[Test]
    public function task_creation_fails_without_required_fields()
    {
        $user = UserFactory::new()->create();

        $this->actingAs($user);

        $this->postJson(route('tasks.store'), [
            'description' => 'Task description'
        ])->assertStatus(422);
    }

    #[Test]
    public function unauthenticated_user_cannot_create_task()
    {
        $this->postJson(route('tasks.store'), [
            'title' => 'Task title',
            'description' => 'Task description',
        ])->assertStatus(401);
    }
}
