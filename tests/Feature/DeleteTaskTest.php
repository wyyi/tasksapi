<?php

declare(strict_types=1);

namespace Tests\Feature;

use Tests\TestCase;
use Database\Factories\TaskFactory;
use Database\Factories\UserFactory;
use PHPUnit\Framework\Attributes\Test;

class DeleteTaskTest extends TestCase
{
    #[Test]
    public function successfully_deletes_a_task_with_all_its_subtasks()
    {
        $user = UserFactory::new()->create();

        $task = TaskFactory::new()->create(['user_id' => $user->id]);

        $task->subtasks()->saveMany(
            TaskFactory::new()->count(3)->create(['user_id' => $user->id, 'parent_id' => $task->id])
        );

        $this->actingAs($user);

        $response = $this->deleteJson(route('tasks.destroy', $task->id));

        $response->assertStatus(204);

        $this->assertDatabaseMissing('tasks', ['id' => $task->id]);

        $this->assertDatabaseMissing('tasks', ['parent_id' => $task->id]);
    }

    #[Test]
    public function rejects_deletion_attempt_by_unauthorized_user()
    {
        $user = UserFactory::new()->create();

        $task = TaskFactory::new()->create(['user_id' => $user->id]);

        $response = $this->deleteJson(route('tasks.destroy', $task->id));

        $response->assertStatus(401);

        $this->assertDatabaseHas('tasks', ['id' => $task->id]);
    }

    #[Test]
    public function handles_attempt_to_delete_non_existent_task()
    {
        $user = UserFactory::new()->create();

        $this->actingAs($user);

        $response = $this->deleteJson(route('tasks.destroy', 1));

        $response->assertStatus(404);
    }

    #[Test]
    public function a_user_cannot_delete_somebody_elses_task()
    {
        $user = UserFactory::new()->create();

        $task = TaskFactory::new()->create(['user_id' => UserFactory::new()->create()->id]);

        $this->actingAs($user);

        $response = $this->deleteJson(route('tasks.destroy', $task->id));

        $response->assertStatus(403);

        $this->assertDatabaseHas('tasks', ['id' => $task->id]);
    }
}
