<?php

declare(strict_types=1);

namespace Tests\Feature;

use Tests\TestCase;
use Database\Factories\UserFactory;
use PHPUnit\Framework\Attributes\Test;

class ShowTaskTest extends TestCase
{
    #[Test]
    public function successfully_retrieves_a_single_task(): void
    {
        $user = UserFactory::new()->create();

        $task = $user->tasks()->create([
            'title' => 'Test Task',
            'description' => 'This is a test task',
        ]);

        $response = $this->actingAs($user)->get(route('tasks.show', $task->id));

        $response->assertStatus(200);
    }

    #[Test]
    public function returns_not_found_for_nonexistent_tasks(): void
    {
        $user = UserFactory::new()->create();

        $response = $this->actingAs($user)->get(route('tasks.show', 1));

        $response->assertStatus(404);
    }

    #[Test]
    public function rejects_unauthorized_access(): void
    {
        $user = UserFactory::new()->create();
        $otherUser = UserFactory::new()->create();

        $task = $user->tasks()->create([
            'title' => 'Test Task',
            'description' => 'This is a test task',
        ]);

        $response = $this->actingAs($otherUser)->get(route('tasks.show', $task->id));

        $response->assertStatus(403);
    }
}
