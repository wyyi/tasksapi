<?php

declare(strict_types=1);

namespace Tests\Feature;

use Tests\TestCase;
use Database\Factories\TaskFactory;
use Database\Factories\UserFactory;
use PHPUnit\Framework\Attributes\Test;

class UpdateTaskTest extends TestCase
{
    #[Test]
    public function updates_status_successfully_when_status_field_is_provided()
    {
        $user = UserFactory::new()->create();

        $this->actingAs($user);

        $task = TaskFactory::new()->create([
            'user_id' => $user->id,
            'status' => 'todo'
        ]);

        $response = $this->patchJson(route('tasks.update', $task->id), ['status' => 'done']);

        $response->assertStatus(200);

        $response->assertJsonStructure([
            'data' => ['id', 'title', 'description', 'status', 'priority', 'parent_id', 'created_at', 'updated_at']
        ]);

        $response->assertJson([
            'data' => ['status' => 'done']
        ]);
    }

    #[Test]
    public function ignores_other_fields_when_status_field_is_provided()
    {
        $user = UserFactory::new()->create();

        $this->actingAs($user);

        $task = TaskFactory::new()->create([
            'user_id' => $user->id,
            'title' => 'A very important task',
            'description' => 'Very important task description',
            'status' => 'todo'
        ]);

        $response = $this->patchJson(route('tasks.update', $task->id), [
            'status' => 'done',
            'title' => 'Updated title',
            'description' => 'Updated description'
        ]);

        $response->assertStatus(200);

        $response->assertJsonStructure([
            'data' => ['id', 'title', 'description', 'status', 'priority', 'parent_id', 'created_at', 'updated_at']
        ]);

        $response->assertJson([
            'data' => ['status' => 'done', 'title' => $task->title, 'description' => $task->description]
        ]);

        $this->assertDatabaseMissing('tasks', [
            'id' => $task->id,
            'title' => 'Updated title',
            'description' => 'Updated description'
        ]);
    }

    #[Test]
    public function rejects_update_with_invalid_status()
    {
        $user = UserFactory::new()->create();

        $this->actingAs($user);

        $task = TaskFactory::new()->create(['user_id' => $user->id, 'status' => 'todo']);

        $response = $this->patchJson(route('tasks.update', $task->id), [
            'status' => 'some made up status'
        ]);

        $response->assertStatus(422);
    }

    #[Test]
    public function updates_parent_successfully_when_parent_id_field_is_provided()
    {
        $user = UserFactory::new()->create();

        $this->actingAs($user);

        $task = TaskFactory::new()->create(['user_id' => $user->id, 'status' => 'todo']);

        $anotherTask = TaskFactory::new()->create(['user_id' => $user->id, 'status' => 'todo']);

        $response = $this->patchJson(route('tasks.update', $task->id), [
            'parent_id' => $anotherTask->id
        ]);

        $response->assertStatus(200);

        $response->assertJsonStructure([
            'data' => ['id', 'title', 'description', 'status', 'priority', 'parent_id', 'created_at', 'updated_at']
        ]);

        $response->assertJson([
            'data' => ['parent_id' => $anotherTask->id]
        ]);
    }

    #[Test]
    public function rejects_update_with_invalid_parent_id()
    {
        $user = UserFactory::new()->create();

        $this->actingAs($user);

        $task = TaskFactory::new()->create(['user_id' => $user->id, 'status' => 'todo']);

        $response = $this->patchJson(route('tasks.update', $task->id), [
            'parent_id' => 999
        ]);

        $response->assertStatus(404);
    }

    #[Test]
    public function rejects_update_when_parent_task_belongs_to_somebody_else()
    {
        $user = UserFactory::new()->create();

        $task = TaskFactory::new()->create(['user_id' => $user->id]);

        $anotherUser = UserFactory::new()->create();

        $anotherUsersTask = TaskFactory::new()->create(['user_id' => $anotherUser->id]);

        $this->actingAs($user);

        $response = $this->patchJson(route('tasks.update', $task->id), [
            'parent_id' => $anotherUsersTask->id
        ]);

        $response->assertStatus(403);
    }

    #[Test]
    public function updates_task_info_when_neither_status_nor_parent_id_is_provided()
    {
        $user = UserFactory::new()->create();

        $this->actingAs($user);

        $task = TaskFactory::new()->create([
            'user_id' => $user->id,
            'title' => 'A very important task',
            'description' => 'Very important task description',
            'status' => 'todo'
        ]);

        $response = $this->patchJson(route('tasks.update', $task->id), [
            'title' => 'Updated title',
            'description' => 'Updated description',
            'priority' => '5'
        ]);

        $response->assertStatus(200);

        $this->assertDatabaseHas('tasks', [
            'id' => $task->id,
            'title' => 'Updated title',
            'description' => 'Updated description'
        ]);
    }

    #[Test]
    public function rejects_update_with_invalid_priority()
    {
        $user = UserFactory::new()->create();

        $this->actingAs($user);

        $task = TaskFactory::new()->create(['user_id' => $user->id]);

        $response = $this->patchJson(route('tasks.update', $task->id), [
            'priority' => '7'
        ]);

        $response->assertStatus(422);
    }

    #[Test]
    public function a_user_cannot_update_somebody_elses_task()
    {
        $user = UserFactory::new()->create();

        $anotherUser = UserFactory::new()->create();

        $this->actingAs($user);

        $task = TaskFactory::new()->create([
            'user_id' => $anotherUser->id,
            'status' => 'todo'
        ]);

        $response = $this->patchJson(route('tasks.update', $task->id), [
            'status' => 'done'
        ]);

        $response->assertStatus(403);
    }

    #[Test]
    public function an_unauthenticated_user_cannot_update_task()
    {
        $user = UserFactory::new()->create();

        $task = TaskFactory::new()->create(['user_id' => $user->id]);

        $this->patchJson(route('tasks.update', $task->id), [
            'status' => 'done'
        ])->assertStatus(401);
    }
}
