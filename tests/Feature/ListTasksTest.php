<?php

declare(strict_types=1);

namespace Tests\Feature;

use Tests\TestCase;
use Database\Factories\TaskFactory;
use Database\Factories\UserFactory;
use PHPUnit\Framework\Attributes\Test;

class ListTasksTest extends TestCase
{
    #[Test]
    public function retrieves_all_tasks_successfully()
    {
        $user = UserFactory::new()->create();

        $this->actingAs($user);

        $response = $this->getJson(route('tasks.index'));

        $response->assertStatus(200);
    }

    #[Test]
    public function retrieves_filtered_tasks_successfully()
    {
        $user = UserFactory::new()->create();

        $user->tasks()->saveMany([
            TaskFactory::new()->make(['status' => 'done', 'priority' => 1, 'title' => 'read something']),
            TaskFactory::new()->make(['status' => 'done', 'priority' => 2, 'title' => 'write something']),
            TaskFactory::new()->make(['status' => 'done', 'priority' => 1, 'title' => 'test something']),
            TaskFactory::new()->make(['status' => 'todo', 'priority' => 2, 'title' => 'test something else']),
        ]);

        $this->actingAs($user);

        $response = $this->getJson(route('tasks.index', ['filter' => ['status' => 'done', 'priority' => 1]]));
        $response->assertJsonCount(2, 'data');

        $response->assertStatus(200);
    }

    #[Test]
    public function retrieves_sorted_tasks_successfully()
    {
        $user = UserFactory::new()->create();

        $user->tasks()->saveMany([
            TaskFactory::new()->make(['priority' => 1, 'title' => 'read something', 'completed_at' => now()->subDays(1)]),
            TaskFactory::new()->make(['priority' => 2, 'title' => 'write something', 'completed_at' => now()->subDays(2)]),
            TaskFactory::new()->make(['priority' => 5, 'title' => 'test something', 'completed_at' => now()->subDays(3)]),
            TaskFactory::new()->make(['priority' => 2, 'title' => 'test something else', 'completed_at' => now()->subDays(4)]),
        ]);

        $this->actingAs($user);

        $response = $this->getJson(route('tasks.index', ['sort' => ['priority' => 'desc', 'completed_at' => 'asc']]));

        $response->assertJsonPath('data.0.title', 'test something');
        $response->assertJsonPath('data.1.title', 'test something else');
        $response->assertJsonPath('data.2.title', 'write something');
        $response->assertJsonPath('data.3.title', 'read something');

        $response->assertStatus(200);
    }
}
