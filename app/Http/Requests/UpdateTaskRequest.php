<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\ValidationRule;

class UpdateTaskRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'status' => ['nullable', 'string', 'in:todo,done'],
            'parent_id' => ['nullable', 'integer'],
            'title' => ['nullable', 'string', 'max:255', 'required_without_all:status,parent_id'],
            'description' => ['nullable', 'string', 'required_without_all:status,parent_id'],
            'priority' => ['nullable', 'integer', 'between:1,5', 'required_without_all:status,parent_id']
        ];
    }
}
