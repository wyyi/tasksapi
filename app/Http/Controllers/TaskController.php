<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Domain\Models\Task;
use Illuminate\Http\Request;
use App\Domain\DTOs\SortOption;
use App\Domain\DTOs\CreateTask;
use App\Domain\DTOs\UpdateTask;
use App\Domain\Models\TaskStatus;
use Illuminate\Http\JsonResponse;
use App\Domain\DTOs\FilterOptions;
use Illuminate\Support\Collection;
use App\Domain\Models\TaskPriority;
use App\Http\Resources\TaskResource;
use App\Http\Requests\StoreTaskRequest;
use App\Domain\Services\AddTaskService;
use App\Http\Requests\UpdateTaskRequest;
use App\Domain\Repositories\TaskRepository;
use App\Domain\Services\UpdateTaskInfoService;
use App\Domain\Services\ChangeTaskStatusService;
use App\Domain\Services\UpdateTaskParentService;
use Illuminate\Auth\Access\AuthorizationException;

class TaskController extends Controller
{
    public function __construct(
        private readonly AddTaskService $addTaskService,
        private readonly TaskRepository $taskRepository,
        private readonly ChangeTaskStatusService $changeTaskStatusService,
        private readonly UpdateTaskInfoService $updateTaskInfoService,
        private readonly UpdateTaskParentService $updateTaskParentService,
    ) {
    }

    public function index(Request $request): JsonResponse
    {
        $user = $request->user();

        $filterRequest = Collection::make($request->get('filter', []))->filter();

        $sortRequest = Collection::make($request->get('sort', []))->filter(function (string $direction, string $field) {
            return in_array($field, SortOption::FIELDS);
        });

        $sortOptions = $sortRequest->map(fn($direction, $field) => new SortOption($field, $direction));

        if ($filterRequest->isEmpty()) {
            $tasks = $this->taskRepository->getTopLevelTaskTrees($user, $sortOptions);

            return $this->successResponse(TaskResource::collection($tasks));
        }

        $filterOptions = new FilterOptions(
            status: TaskStatus::tryFrom($filterRequest->get('status', '')),
            priority: TaskPriority::tryFrom((int) $filterRequest->get('priority', 0)),
            searchText: $filterRequest->get('search')
        );

        $tasks = $this->taskRepository->listTasksAcrossLevels($user, $sortOptions, $filterOptions);

        return $this->successResponse(TaskResource::collection($tasks));
    }

    public function show(Request $request, Task $task): JsonResponse
    {
        if ($request->user()->cannot('view', $task)) {
            throw new AuthorizationException();
        }

        $task->loadFullSubtaskTree();

        return $this->successResponse(new TaskResource($task));
    }

    public function store(StoreTaskRequest $request): JsonResponse
    {
        $createTaskData = new CreateTask(
            title: $request->get('title'),
            priority: (int) $request->get('priority'),
            parentId: $request->has('parent_id') ? (int) $request->input('parent_id') : null,
            description: $request->get('description')
        );

        $task = $this->addTaskService->execute($request->user(), $createTaskData);

        return $this->createdResponse(new TaskResource($task));
    }

    public function update(UpdateTaskRequest $request, Task $task): JsonResponse
    {
        if ($request->user()->cannot('update', $task)) {
            return $this->unauthorizedResponse();
        }

        if ($request->has('status')) {
            $task = $this->changeTaskStatusService->execute($task, TaskStatus::from($request->get('status')));

            return $this->successResponse(new TaskResource($task));
        }

        if ($request->has('parent_id')) {
            $task = $this->updateTaskParentService->execute($request->user(), $task, (int) $request->get('parent_id'));

            return $this->successResponse(new TaskResource($task));
        }

        $updateTask = new UpdateTask(
            title: $request->get('title'),
            description: $request->get('description'),
            priority: $request->has('priority') ? (int) $request->get('priority') : null
        );

        $task = $this->updateTaskInfoService->execute($task, $updateTask);

        return $this->successResponse(new TaskResource($task));
    }

    public function destroy(Task $task): JsonResponse
    {
        if (auth()->user()->cannot('delete', $task)) {
            return $this->unauthorizedResponse();
        }

        $this->taskRepository->deleteWithAllSubtasks($task);

        return $this->emptyResponse();
    }
}
