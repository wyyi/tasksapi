<?php

namespace App\Http\Controllers;

use JsonSerializable;
use Illuminate\Http\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

abstract class Controller
{
    public function successResponse(JsonSerializable|array $data): JsonResponse
    {
        return new JsonResponse(['data' => $data]);
    }

    public function unauthorizedResponse(): JsonResponse
    {
        return new JsonResponse(null, Response::HTTP_FORBIDDEN);
    }

    public function emptyResponse(): JsonResponse
    {
        return new JsonResponse(null, Response::HTTP_NO_CONTENT);
    }

    public function createdResponse(JsonSerializable $item): JsonResponse
    {
        return new JsonResponse(['data' => $item], Response::HTTP_CREATED);
    }
}
