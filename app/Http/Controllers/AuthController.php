<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Domain\Models\User;
use Illuminate\Http\Request;
use Illuminate\Auth\AuthManager;
use Illuminate\Http\JsonResponse;
use App\Http\Requests\LoginRequest;
use Illuminate\Hashing\BcryptHasher;
use App\Http\Requests\RegisterRequest;
use App\Domain\Repositories\UserRepository;
use Illuminate\Validation\ValidationException;

class AuthController extends Controller
{
    public function __construct(
        private readonly UserRepository $userRepository,
        private readonly AuthManager $authManager,
        private readonly BcryptHasher $hasher
    ) {
    }

    public function register(RegisterRequest $request): JsonResponse
    {
        $user = new User([
            'name' => $request->input('name'),
            'email' => $request->input('email'),
            'password' => $this->hasher->make($request->input('password'))
        ]);

        $this->userRepository->save($user);

        return $this->createdResponse($user);
    }

    public function login(LoginRequest $request): JsonResponse
    {
        $isSuccessfulAttempt = $this->authManager->attempt($request->only('email', 'password'));

        if (! $isSuccessfulAttempt) {
            throw ValidationException::withMessages(['password' => 'Invalid credentials']);
        }

        $user = $this->userRepository->findByEmail($request->input('email'));

        $authToken = $user->createToken('auth-token')->plainTextToken;

        return $this->successResponse(['access_token' => $authToken]);
    }

    public function logout(Request $request): JsonResponse
    {
        $request->user()->currentAccessToken()->delete();

        return $this->emptyResponse();
    }
}
