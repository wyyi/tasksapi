<?php

declare(strict_types=1);

namespace App\Domain\Repositories;

use App\Domain\Models\Task;
use App\Domain\Models\User;
use App\Domain\DTOs\SortOption;
use App\Domain\DTOs\FilterOptions;
use Illuminate\Support\Collection;
use Illuminate\Database\Eloquent\Builder;

class TaskRepository
{
    public function getById(int $id): Task
    {
        return Task::findOrFail($id);
    }

    public function getTopLevelTaskTrees(User $user, Collection $sortOptions): Collection
    {
        $query = Task::query()->with('fullSubtaskTree');

        $this->scopeToTopLevel($query);

        $this->scopeToUser($query, $user);

        $this->applySorting($query, $sortOptions);

        return $query->get();
    }

    public function listTasksAcrossLevels(User $user, Collection $sortOptions, FilterOptions $filterOptions): Collection
    {
        $query = Task::query();

        $this->scopeToUser($query, $user);

        $this->applyFilters($query, $filterOptions);

        $this->applySorting($query, $sortOptions);

        return $query->get();
    }

    public function deleteWithAllSubtasks(Task $task): bool
    {
        return $task->delete();
    }

    public function save(Task $task): Task
    {
        $task->save();

        return $task;
    }

    private function scopeToTopLevel(Builder $query): void
    {
        $query->whereNull('parent_id');
    }

    private function scopeToUser(Builder $query, User $user): void
    {
        $query->where('user_id', $user->getAttribute('id'));
    }

    private function applySorting(Builder $query, Collection $sortOptions): void
    {
        $sortOptions->each(fn(SortOption $sortOption) => $query->orderBy($sortOption->field, $sortOption->direction));
    }

    private function applyFilters(Builder $query, FilterOptions $filterOptions): void
    {
        if ($filterOptions->status) {
            $query->where('status', $filterOptions->status);
        }

        if ($filterOptions->priority) {
            $query->where('priority', $filterOptions->priority);
        }

        if ($filterOptions->searchText) {
            $query->whereFullText(['title', 'description'], $filterOptions->searchText);
        }
    }
}
