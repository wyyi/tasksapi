<?php

declare(strict_types=1);

namespace App\Domain\Repositories;

use App\Domain\Models\User;

class UserRepository
{
    public function findByEmail(string $email): ?User
    {
        return User::where('email', $email)->first();
    }

    public function save(User $user): void
    {
        $user->save();
    }
}
