<?php

declare(strict_types=1);

namespace App\Domain\Services;

use App\Domain\Models\User;
use App\Domain\Models\Task;
use App\Domain\DTOs\CreateTask;
use App\Domain\Models\TaskStatus;
use App\Domain\Models\TaskPriority;
use App\Domain\Repositories\TaskRepository;
use Illuminate\Validation\ValidationException;
use Illuminate\Auth\Access\AuthorizationException;

readonly class AddTaskService
{
    public function __construct(
        private TaskRepository $taskRepository
    ) {
    }

    public function execute(User $user, CreateTask $data): Task
    {
        $parentTask = $data->parentId !== null ? $this->taskRepository->getById($data->parentId) : null;

        if ($parentTask && $parentTask->doesNotBelongToUser($user)) {
            throw new AuthorizationException('You are not allowed to create a subtask for this task.');
        }

        if ($parentTask && $parentTask->isCompleted()) {
            throw ValidationException::withMessages(['parent_id' => 'You cannot set the parent to a completed task']);
        }

        $newTask =  new Task([
            'title' => $data->title,
            'description' => $data->description,
            'priority' => $data->priority? TaskPriority::from($data->priority) : TaskPriority::default(),
            'status' => TaskStatus::TODO,
            'completed_at' => null,
            'user_id' => $user->getAttribute('id'),
            'parent_id' => $data->parentId,
        ]);

        return $this->taskRepository->save($newTask);
    }
}
