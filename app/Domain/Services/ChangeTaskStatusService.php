<?php

declare(strict_types=1);

namespace App\Domain\Services;

use App\Domain\Models\Task;
use App\Domain\Models\TaskStatus;
use Illuminate\Support\Collection;
use App\Domain\Repositories\TaskRepository;

readonly class ChangeTaskStatusService
{
    public function __construct(
        private TaskRepository $taskRepository
    ) {
    }

    public function execute(Task $task, TaskStatus $status): Task
    {
        if ($status === TaskStatus::DONE) {
            $task->loadFullSubtaskTree();

            if ($this->hasUncompletedSubtasksDownTheTree($task)) {
                throw new \DomainException('Cannot mark task as completed because it has uncompleted subtasks');
            }

            $task->markAsCompleted();
        } else {
            $task->markAsTodo();
        }

        $this->taskRepository->save($task);

        return $task;
    }

    private function hasUncompletedSubtasksDownTheTree(Task $task): bool
    {
        /** @var Collection<Task> $subtasks */
        $subtasks = $task->subtasks;

        if ($this->hasUncompletedDirectSubtasks($task)) {
            return true;
        }

        foreach ($subtasks as $subtask) {
            if ($this->hasUncompletedSubtasksDownTheTree($subtask)) {
                return true;
            }
        }

        return false;
    }

    private function hasUncompletedDirectSubtasks(Task $task): bool
    {
        return $task->subtasks->contains(fn(Task $task) => $task->isNotCompleted());
    }
}
