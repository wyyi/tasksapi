<?php

declare(strict_types=1);

namespace App\Domain\Services;

use App\Domain\Models\Task;
use App\Domain\Models\User;
use App\Domain\Repositories\TaskRepository;
use Illuminate\Validation\ValidationException;
use Illuminate\Auth\Access\AuthorizationException;

readonly class UpdateTaskParentService
{
    public function __construct(
        private TaskRepository $taskRepository
    ) {
    }

    public function execute(User $user, Task $task, int $parentId): Task
    {
        $parentTask = $this->taskRepository->getById($parentId);

        if ($parentTask->doesNotBelongToUser($user)) {
            throw new AuthorizationException('You are not allowed to create a subtask for this task.');
        }

        if ($parentTask->isCompleted()) {
            throw ValidationException::withMessages(['parent_id' => 'You cannot change the parent to a completed task']);
        }

        $parentTask->loadFullSubtaskTree();

        if ($parentTask->isDescendantOf($task))  {
            throw new \DomainException('Cannot set a task as a parent of its descendant');
        }

        $task->changeParent($parentTask);

        $this->taskRepository->save($task);

        return $task;
    }
}
