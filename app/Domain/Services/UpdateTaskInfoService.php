<?php

declare(strict_types=1);

namespace App\Domain\Services;

use App\Domain\Models\Task;
use App\Domain\DTOs\UpdateTask;
use App\Domain\Models\TaskPriority;
use App\Domain\Repositories\TaskRepository;

readonly class UpdateTaskInfoService
{
    public function __construct(
        private TaskRepository $taskRepository
    ) {
    }

    public function execute(Task $task, UpdateTask $updateData): Task
    {
        $task->changeTitle($updateData->title);

        $task->changeDescription($updateData->description);

        $task->changePriority(TaskPriority::from($updateData->priority));

        $this->taskRepository->save($task);

        return $task;
    }
}
