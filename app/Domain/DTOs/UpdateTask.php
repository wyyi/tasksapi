<?php

declare(strict_types=1);

namespace App\Domain\DTOs;

readonly class UpdateTask
{
    public function __construct(
        public string $title,
        public ?string $description,
        public int $priority,
    ) {
    }
}
