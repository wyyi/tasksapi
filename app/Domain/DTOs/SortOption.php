<?php

declare(strict_types=1);

namespace App\Domain\DTOs;

readonly class SortOption
{
    public const array FIELDS = ['created_at', 'completed_at', 'priority'];

    public string $field;
    public string $direction;

    public function __construct(string $field, ?string $direction)
    {
        $this->field = $field;
        $this->direction = $direction === 'desc' ? 'desc' : 'asc';
    }
}
