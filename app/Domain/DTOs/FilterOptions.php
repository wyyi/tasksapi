<?php

declare(strict_types=1);

namespace App\Domain\DTOs;

use App\Domain\Models\TaskStatus;
use App\Domain\Models\TaskPriority;

readonly class FilterOptions
{

    public function __construct(
        public ?TaskStatus $status = null,
        public ?TaskPriority $priority = null,
        public ?string $searchText = null
    ) {
    }
}
