<?php

declare(strict_types=1);

namespace App\Domain\DTOs;

readonly class CreateTask
{
    public function __construct(
        public string $title,
        public ?int $priority,
        public ?int $parentId = null,
        public ?string $description = null
    ) {
    }
}
