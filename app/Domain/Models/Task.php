<?php

declare(strict_types=1);

namespace App\Domain\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Task extends Model
{
    use HasFactory;

    protected $guarded = [];

    protected $casts = [
        'status' => TaskStatus::class,
        'priority' => TaskPriority::class
    ];

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function parent(): BelongsTo
    {
        return $this->belongsTo(Task::class, 'parent_id');
    }

    public function subtasks(): HasMany
    {
        return $this->hasMany(Task::class, 'parent_id');
    }

    public function fullSubtaskTree(): HasMany
    {
        return $this->subtasks()->with('fullSubtaskTree');
    }

    public function loadFullSubtaskTree(): void
    {
        $this->load('fullSubtaskTree');
    }

    public function changePriority(TaskPriority $priority): void
    {
        $this->setAttribute('priority', $priority);
    }

    public function markAsCompleted(): void
    {
        $this->setAttribute('status', TaskStatus::DONE);
        $this->setAttribute('completed_at', now());
    }

    public function markAsTodo(): void
    {
        $this->setAttribute('status', TaskStatus::TODO);
        $this->setAttribute('completed_at', null);
    }

    public function changeTitle(string $title): void
    {
        $this->setAttribute('title', $title);
    }

    public function changeDescription(string $description): void
    {
        $this->setAttribute('description', $description);
    }

    public function changeParent(Task $task): void
    {
        $this->parent()->associate($task);
    }

    public function isDescendantOf(Task $potentialParentTask): bool
    {
        if ($potentialParentTask->isParentOf($this)) {
            return true;
        }

        return $this->parent?->isDescendantOf($potentialParentTask) ?? false;
    }

    private function isParentOf(Task $task): bool
    {
        return $this->getAttribute('id') === $task->getAttribute('parent_id');
    }

    public function doesNotBelongToUser(User $user): bool
    {
        return $this->getAttribute('user_id') !== $user->getAttribute('id');
    }

    public function isCompleted(): bool
    {
        return $this->status === TaskStatus::DONE;
    }

    public function isNotCompleted(): bool
    {
        return ! $this->status->equals(TaskStatus::DONE);
    }
}
